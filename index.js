const dbConnect = require('./schema/db');
const Model = require('./schema/model');
const fs = require('fs');

async function migrate() {
  const db = await dbConnect();

  const { Paper, Chronology, Keyword, Region } = Model;

  const file = fs
    .readFileSync('./BiblioArch Italia.csv', { encoding: 'utf-8' })
    .toString();
  const data = csvToArray(file);

  let regions = new Set();
  let keywords = new Set();
  let chronologies = new Set();

  await Paper.deleteMany();
  await Region.deleteMany();
  await Keyword.deleteMany();
  await Chronology.deleteMany();

  for (let paper of data) {
    if (paper[5]) {
      paper[5].split(';').forEach((region) => regions.add(region.trim()));
    }
    if (paper[6].trim()) {
      paper[6]
        .split(';')
        .forEach((chronology) => chronologies.add(chronology.trim()));
    }
    if (paper[7].trim()) {
      paper[7].split(';').forEach((keyword) => keywords.add(keyword.trim()));
    }
  }

  for (const region of regions) {
    try {
      await new Region({ name: region }).save();
    } catch (e) {
      console.log('duplicate: ', region);
    }
  }
  for (const keyword of keywords) {
    try {
      await new Keyword({ name: keyword }).save();
    } catch (e) {
      console.log('duplicate: ', keyword);
    }
  }
  for (const chronology of chronologies) {
    try {
      await new Chronology({ name: chronology }).save();
    } catch (e) {
      console.log('duplicate: ', chronology);
    }
  }

  console.log(regions);
  console.log(keywords);
  console.log(chronologies);

  await new Keyword({ name: 'No Keywords' }).save();
  await new Region({ name: 'No Regions' }).save();
  await new Chronology({ name: 'No Chronologies' }).save();

  const keywordsDb = await Keyword.find({});
  const regionsDb = await Region.find({});
  const chronologyDb = await Region.find({});

  const papersToSave = data.map((paper) => ({
    authors: paper[0],
    year: paper[1],
    title: paper[2],
    journal_book: paper[3],
    doi: paper[4],
    regions:
      regionsDb.filter((region) => paper[5]?.includes(region.name)).length === 0
        ? regionsDb.filter((region) => region.name === 'No Regions')
        : regionsDb.filter((region) => paper[5]?.includes(region.name)),
    chronology:
      chronologyDb.filter((chronology) => paper[6]?.includes(chronology.name))
        .length === 0
        ? chronologyDb.filter(
            (chronology) => chronology.name === 'No Chronologies'
          )
        : chronologyDb.filter((chronology) =>
            paper[6]?.includes(chronology.name)
          ),
    keywords:
      keywordsDb.filter((keyword) => paper[7]?.includes(keyword.name))
        .length === 0
        ? keywordsDb.filter((keyword) => keyword.name === 'No Keywords')
        : keywordsDb.filter((keyword) => paper[7]?.includes(keyword.name)),
    abstract: paper[8],
  }));

  await Paper.insertMany(papersToSave);
  db.connection.close();
  console.log('done');
}

migrate();

function csvToArray(text) {
  let p = '',
    row = [''],
    ret = [row],
    i = 0,
    r = 0,
    s = !0,
    l;
  for (l of text) {
    if ('"' === l) {
      if (s && l === p) row[i] += l;
      s = !s;
    } else if (',' === l && s) l = row[++i] = '';
    else if ('\n' === l && s) {
      if ('\r' === p) row[i] = row[i].slice(0, -1);
      row = ret[++r] = [(l = '')];
      i = 0;
    } else row[i] += l;
    p = l;
  }
  return ret;
}
